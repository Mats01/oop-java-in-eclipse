package hr.fer.oop.lab2.prob5;



public class Shapes {
	public static String plus =    "+--------+";
	public static String bottom1 = "\\        /";
	public static String bottom2 = " \\______/ ";
	public static String crtica =   "  ______";
	public static String top1 =    " /      \\ ";
	public static String top2 =    "/        \\ ";
	public static void main(String[] args) {
		Shapes.printPlus();
		Shapes.printBottom();
		
		Shapes.printTop();
		Shapes.printPlus();
		Shapes.printTop();
		
		Shapes.printHex();
		
		Shapes.printPlus();
		Shapes.printTop();
		Shapes.printPlus();
		
	}
	
	public static void printHex() {
		Shapes.printBottom();
		System.out.println();
		Shapes.printBottom();
	}
	
	public static void printPlus() {
		System.out.println(plus);
	}
	public static void printTop() {
		System.out.println(crtica);
		System.out.println(top1);
		System.out.println(top2);
	}
	public static void printBottom() {
		System.out.println(bottom1);
		System.out.println(bottom2);

	}

}
