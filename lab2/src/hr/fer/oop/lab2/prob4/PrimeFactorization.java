package hr.fer.oop.lab2.prob4;

public class PrimeFactorization {
	public static int numerator;
	public static void main(String[] args) {
		int numbercicko = Integer.parseInt(args[0]);
		int smallDiv;
		while((smallDiv = PrimeFactorization.smallestPrimeFactor(numbercicko)) != 1) {
			numbercicko = numbercicko / smallDiv;	
		}
		numerator++;
		System.out.println(numerator + ". " + numbercicko);
	}
	
	public static int smallestPrimeFactor(int n) {
		for(int i = 2; i < n; i++) {
			if(PrimeFactorization.isPrime(i) && (n % i == 0)) {
				numerator++;
				System.out.println(numerator + ". " +  String.valueOf(i));
				return i;
			}
		}
		return 1;
	}
	
	public static boolean isPrime(int n) {
		for(int j = n-1; j>=1; j--) {
			if(j == 1) {
				return true;
			}
			if(n%j == 0) {
				break;
			}
		}
		return false;
		
	}
	

}
