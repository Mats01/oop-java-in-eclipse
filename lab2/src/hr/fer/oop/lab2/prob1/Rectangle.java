package hr.fer.oop.lab2.prob1;
import java.util.Scanner;

public class Rectangle {
	private static Scanner scannedObj;
	public static void main(String[] args) {
		float height;
		float width;
		if(args.length == 2) {
			width = Float.parseFloat(args[0]);
			height = Float.parseFloat(args[1]);
			Rectangle.finalPrint(height, width);
			
		}
		else if(args.length > 0) {
			System.out.println("Invalid number of arguments was provided.");
		}
		else {
			scannedObj = new Scanner(System.in);
			height = Rectangle.getIntValue("height");
			width = Rectangle.getIntValue("width");
			Rectangle.finalPrint(height, width);
			
			scannedObj.close();
		}

	}
	public static void finalPrint(float height, float width) {
		String finalOutput = "You have specified a rectangle of width " + width + 
				" and height " + height + ". Its area is " + (height*width) + 
				" and its perimeter is " + (2*width+2*height);
		System.out.println(finalOutput);
		
	}
	public static String evaluateInput(String input, String parameter) {
		if(input.isEmpty()) {
			return new String(parameter + " cannot be empty"); 
		}
		float userInput = Float.parseFloat(input);
		
		if(userInput <= 0) {
			return new String(parameter + " is zero or less");
		}
		return new String();
	}
	public static Float getIntValue(String parameter) {
		System.out.println("Please provide " + parameter + ":");
		
		String scanned = scannedObj.nextLine();
		String result;
		while(!(result = Rectangle.evaluateInput(scanned, parameter)).isEmpty()) {
			System.out.println(result);
			scanned = Rectangle.scannedObj.nextLine();
			
		}
		return Float.parseFloat(scanned);
	}

}
