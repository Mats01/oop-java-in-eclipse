package hr.fer.oop.lab2.prob2;

public class Roots {
	public static void main(String[] args) {
		int ReZ = Integer.parseInt(args[0]);
		int ImZ = Integer.parseInt(args[1]);
		int root = Integer.parseInt(args[2]);
		double div = (double) ImZ/ReZ;
		double fi = Math.atan(div);
		
		double r = (ReZ*ReZ + ImZ*ImZ);
		r = Math.pow(r, 1.0/2);
		
		
		r = Math.pow(r, 1.0/root);
		
		fi = fi/root;
		System.out.println("You requested calculation of " + root +  ". roots. Solutions are:");
		for (int i = 0; i < root; i++) {
			double add_pi = (double) i*2*Math.PI/root;
			int a = (int) Math.round(r * Math.cos(fi + add_pi)); 
			int b = (int) Math.round(r * Math.sin(fi + add_pi));
			String output = String.valueOf(a);
			if(ReZ < 0) {
				output += " - ";	
				
			}
			else {
				output += " + ";
			}
			output += String.valueOf(Math.abs(b)) + "i";
			
			System.out.println(output);
		}
		
		
	}

}
