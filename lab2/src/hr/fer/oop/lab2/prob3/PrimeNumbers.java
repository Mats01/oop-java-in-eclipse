package hr.fer.oop.lab2.prob3;

public class PrimeNumbers {
	public static void main(String[] args) {
		int N = Integer.parseInt( args[0]);
		int i = 0;
		int primes = 0;
		
		System.out.println("You requested calculation of first " + args[0] + " prime numbers. Here they are:");
		while(primes<N) {
			if(i<2) {
				i++;
				continue;
			}
			/***
			for(int j = i-1; j>=1; j--) {
				if(j == 1) {
					primes++;
					System.out.println(primes + ". " + i);
				}
				if(i%j == 0) {
					break;
				}
			}
			***/
			block: { /* look! for else in java! */
				for(int j = i-1; j>1; j--) {
					if(i%j == 0) {
						break block;
					}
				}
				primes++;
				System.out.println(primes + ". " + i);
				
			}
			
			i++;
			
			
		}
		
	}

}
