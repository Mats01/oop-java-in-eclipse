package hr.fer.oop.lab6.task1;

import java.util.Date;

public class DataIngestClass {
	
	public DataIngestClass()
	{
		//empty constructor
	}
	
	public DataIngestClass(String medallion, String hack_license, String pickup_datetime, String dropoff_datetime,
			int trip_time_in_secs, double trip_distance, double pickup_longitude, double pickup_latitude,
			double dropoff_longitude, double dropoff_latitude, String payment_type, double fare_amount, double surcharge,
			double mta_tax, double tip_amount, double tolls_amount, double total_amount) {
		super();
		this.medallion = medallion;
		this.hack_license = hack_license;
		this.pickup_datetime = pickup_datetime;
		this.dropoff_datetime = dropoff_datetime;
		this.trip_time_in_secs = trip_time_in_secs;
		this.trip_distance = trip_distance;
		this.pickup_longitude = pickup_longitude;
		this.pickup_latitude = pickup_latitude;
		this.dropoff_longitude = dropoff_longitude;
		this.dropoff_latitude = dropoff_latitude;
		this.payment_type = payment_type;
		this.fare_amount = fare_amount;
		this.surcharge = surcharge;
		this.mta_tax = mta_tax;
		this.tip_amount = tip_amount;
		this.tolls_amount = tolls_amount;
		this.total_amount = total_amount;
	}
	public String getMedallion() {
		return medallion;
	}
	public String getHack_license() {
		return hack_license;
	}
	public String getPickup_datetime() {
		return pickup_datetime;
	}
	public String getDropoff_datetime() {
		return dropoff_datetime;
	}
	public int getTrip_time_in_secs() {
		return trip_time_in_secs;
	}
	public double getTrip_distance() {
		return trip_distance;
	}
	public double getPickup_longitude() {
		return pickup_longitude;
	}
	public double getPickup_latitude() {
		return pickup_latitude;
	}
	public double getDropoff_longitude() {
		return dropoff_longitude;
	}
	public double getDropoff_latitude() {
		return dropoff_latitude;
	}
	public String getPayment_type() {
		return payment_type;
	}
	public double getFare_amount() {
		return fare_amount;
	}
	public double getSurcharge() {
		return surcharge;
	}
	public double getMta_tax() {
		return mta_tax;
	}
	public double getTip_amount() {
		return tip_amount;
	}
	public double getTolls_amount() {
		return tolls_amount;
	}
	public double getTotal_amount() {
		return total_amount;
	}
	public void setMedallion(String medallion) {
		this.medallion = medallion;
	}
	public void setHack_license(String hack_license) {
		this.hack_license = hack_license;
	}
	public void setPickup_datetime(String pickup_datetime) {
		this.pickup_datetime = pickup_datetime;
	}
	public void setDropoff_datetime(String dropoff_datetime) {
		this.dropoff_datetime = dropoff_datetime;
	}
	public void setTrip_time_in_secs(int trip_time_in_secs) {
		this.trip_time_in_secs = trip_time_in_secs;
	}
	public void setTrip_distance(double trip_distance) {
		this.trip_distance = trip_distance;
	}
	public void setPickup_longitude(double pickup_longitude) {
		this.pickup_longitude = pickup_longitude;
	}
	public void setPickup_latitude(double pickup_latitude) {
		this.pickup_latitude = pickup_latitude;
	}
	public void setDropoff_longitude(double dropoff_longitude) {
		this.dropoff_longitude = dropoff_longitude;
	}
	public void setDropoff_latitude(double dropoff_latitude) {
		this.dropoff_latitude = dropoff_latitude;
	}
	public void setPayment_type(String payment_type) {
		this.payment_type = payment_type;
	}
	public void setFare_amount(double fare_amount) {
		this.fare_amount = fare_amount;
	}
	public void setSurcharge(double surcharge) {
		this.surcharge = surcharge;
	}
	public void setMta_tax(double mta_tax) {
		this.mta_tax = mta_tax;
	}
	public void setTip_amount(double tip_amount) {
		this.tip_amount = tip_amount;
	}
	public void setTolls_amount(double tolls_amount) {
		this.tolls_amount = tolls_amount;
	}
	public void setTotal_amount(double total_amount) {
		this.total_amount = total_amount;
	}
	private String medallion;
	private String hack_license;
	private String pickup_datetime;
	private String dropoff_datetime;
	private int trip_time_in_secs;
	private double trip_distance;
	private double pickup_longitude;
	private double pickup_latitude;
	private double dropoff_longitude;
	private double dropoff_latitude;
	private String payment_type;
	private double fare_amount;
	private double surcharge;
	private double mta_tax;
	private double tip_amount;
	private double tolls_amount;
	private double total_amount;
	
	
}