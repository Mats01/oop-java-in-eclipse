package hr.fer.oop.lab6.task1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

import javax.swing.SwingWorker;

public class LoadFileWorker extends SwingWorker<List<DataIngestClass>, DataIngestClass> {
	
	private final String fileLocation;
	private Consumer<List<DataIngestClass>> chunksProcessor;
	private Runnable onDone;

		
	    /**
	     * @param numberOfPrimes Number of prime numbers to calculate
	     * @param chunksProcessor consumer of chunks produced by this worker
	     * @param onDone an action to be run after this worker is finished
	     */
	    public LoadFileWorker(String fileLocation, 
	    		Consumer<List<DataIngestClass>> chunksProcessor,
	    		Runnable onDone
	    		) 
	    {
	        this.fileLocation = fileLocation;
			this.chunksProcessor = chunksProcessor;
			this.onDone = onDone;
			
	    }

	    @Override
	    protected List<DataIngestClass> doInBackground() throws Exception {
	    	
	    	List<DataIngestClass> recordList = new ArrayList<>();
			
			//"/home/matej/Documents/FER/JavaOOP/test.txt"
			File file = new File(this.fileLocation); 
			  
			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(file));
			} catch (FileNotFoundException e1) {
				//logText.setText(String.format("%s %s \n", logText.getText(), "No files matched specified path"));
				e1.printStackTrace();
			} 
			  
			String st; 
			try {
				while ((st = br.readLine()) != null) {
					recordList.add(createObjectFromRow(st));
					publish((DataIngestClass) createObjectFromRow(st));
					
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			
			return recordList;
	    }

	    @Override
	    protected void process(List<DataIngestClass> chunks) {
	        chunksProcessor.accept(chunks);
	    }

	    @Override
	    protected void done() {
	        onDone.run();
	    }
	    
	    private DataIngestClass createObjectFromRow(String st){
			String[] attributes = st.split(",");
			DataIngestClass result = new DataIngestClass();
			result.setMedallion(attributes[0]);
			result.setHack_license(attributes[1]);
			result.setPickup_datetime(attributes[2]);
			result.setDropoff_datetime(attributes[3]);
			result.setTrip_time_in_secs(Integer.valueOf(attributes[4]));
			result.setTrip_distance(Double.valueOf(attributes[5]));
			result.setPickup_longitude(Double.valueOf(attributes[6]));
			result.setPickup_latitude(Double.valueOf(attributes[7]));
			result.setDropoff_longitude(Double.valueOf(attributes[8]));
			result.setDropoff_latitude(Double.valueOf(attributes[9]));
			result.setPayment_type(attributes[10]);
			result.setFare_amount(Double.valueOf(attributes[11]));
			result.setSurcharge(Double.valueOf(attributes[12]));
			result.setMta_tax(Double.valueOf(attributes[13]));
			result.setTip_amount(Double.valueOf(attributes[14]));
			result.setTolls_amount(Double.valueOf(attributes[15]));
			result.setTotal_amount(Double.valueOf(attributes[16]));
			
			return result;
		}
	}