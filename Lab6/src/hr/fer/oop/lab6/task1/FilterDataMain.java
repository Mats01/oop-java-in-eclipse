package hr.fer.oop.lab6.task1;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;


public class FilterDataMain {
	public static JTextArea logText = new JTextArea();
	public static JButton applyFilterButton = new JButton();

	public static void main(String[] args) {
		
		
		
		SwingUtilities.invokeLater(() -> {
		    JFrame window = new JFrame();
		    window.setLocation(20, 20);
		    window.setSize(200, 120);
		    window.setVisible(true);
		    window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		    window.setTitle("Generic Table Demonstrator");
		    
		    JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		    window.add(splitPane, BorderLayout.CENTER);
		    
		    GenericTablePanel<DataIngestClass> tablePanel = new GenericTablePanel<>(DataIngestClass.class);
		    tablePanel.setPreferredSize(new Dimension(200, 100));
		    JScrollPane sp = new JScrollPane(logText); 
		    sp.setPreferredSize(new Dimension(1000, 120));
		    tablePanel.add(sp, BorderLayout.SOUTH);
		    splitPane.setBottomComponent(tablePanel);
		    
		    JToolBar toolBar = new JToolBar();
		    JTextField pathField = new JTextField();
		    JButton loadFileButton = new JButton();
		    loadFileButton.setText("Load");
		    loadFileButton.addActionListener(new ActionListener() {
	            @Override
	            public void actionPerformed(ActionEvent actionEvent) {
	                try {
				    	//"/home/matej/Documents/FER/JavaOOP/data_small.csv"
				    	//List<DataIngestClass> recordList = readFileContent(pathField.getText());    
				        //tablePanel.update(recordList);
				        //print tabe content to the console
				        
	                	applyFilterButton.setEnabled(false);
			        	List<DataIngestClass> recordsInTableList = tablePanel.getRecords();
			        	Runnable onDone = () -> applyFilterButton.setEnabled(true);
		                Consumer<List<DataIngestClass>> processChunks = chunks -> {  
		                	 tablePanel.update(chunks);
		                };
		                
		                SwingWorker worker = new LoadFileWorker("/home/matej/Documents/FER/JavaOOP/data_small.csv", processChunks, onDone);
		               
		                worker.execute();
		                
		                
		                
			        	
			        	
			        	int count = recordsInTableList.size();
				        logText.setText(String.format("%s Loaded %d rows \n", logText.getText(), count));
	                } catch (NumberFormatException ex) {
	                    //do nothing
	                }
	            }
				       
		        
	        });
		    	
		    	
		        
		        
		   
		        
		        
		    	
		    
		    JButton defineFilterButton = new JButton();
		    defineFilterButton.addActionListener((e) -> {
		    	logText.setText(String.format("%s %s \n", logText.getText(), "you clicked the Define filter button"));
		    });
		    defineFilterButton.setText("Define filter");
		   
		    applyFilterButton.addActionListener((e) -> {
		    	logText.setText(String.format("%s %s \n", logText.getText(), "you clicked the Apply filter button"));
		    });
		    applyFilterButton.setText("Apply filter");
		    
		    toolBar.add(pathField);
		    toolBar.add(loadFileButton);
		    toolBar.add(defineFilterButton);
		    toolBar.add(applyFilterButton);
		    splitPane.setTopComponent(toolBar);
		    
		    
		    
		    
		    //define the table size and add it to the frame center
		   

		    window.pack();
		});

	
		
		
			
	
	}
	
	static public List<DataIngestClass> readFileContent(String fileLocation) {
		
		List<DataIngestClass> recordList = new ArrayList<>();
		
		//"/home/matej/Documents/FER/JavaOOP/test.txt"
		File file = new File(fileLocation); 
		  
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e1) {
			logText.setText(String.format("%s %s \n", logText.getText(), "No files matched specified path"));
			e1.printStackTrace();
		} 
		  
		String st; 
		try {
			while ((st = br.readLine()) != null) {
				recordList.add(createObjectFromRow(st));
				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return recordList;

		
	}
	
	static public DataIngestClass createObjectFromRow(String st){
		String[] attributes = st.split(",");
		DataIngestClass result = new DataIngestClass();
		result.setMedallion(attributes[0]);
		result.setHack_license(attributes[1]);
		result.setPickup_datetime(attributes[2]);
		result.setDropoff_datetime(attributes[3]);
		result.setTrip_time_in_secs(Integer.valueOf(attributes[4]));
		result.setTrip_distance(Double.valueOf(attributes[5]));
		result.setPickup_longitude(Double.valueOf(attributes[6]));
		result.setPickup_latitude(Double.valueOf(attributes[7]));
		result.setDropoff_longitude(Double.valueOf(attributes[8]));
		result.setDropoff_latitude(Double.valueOf(attributes[9]));
		result.setPayment_type(attributes[10]);
		result.setFare_amount(Double.valueOf(attributes[11]));
		result.setSurcharge(Double.valueOf(attributes[12]));
		result.setMta_tax(Double.valueOf(attributes[13]));
		result.setTip_amount(Double.valueOf(attributes[14]));
		result.setTolls_amount(Double.valueOf(attributes[15]));
		result.setTotal_amount(Double.valueOf(attributes[16]));
		
		return result;
	}
	
	

}

class MyFrame extends JFrame {
	JButton button = new JButton();
	JProgressBar pBar = new JProgressBar();
	JTextField fileLocation = new JTextField();
	int counter = 0;
	
	public MyFrame () {
		super("testing progress");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		
		pBar.setMaximum(100);
		pBar.setMinimum(0);
		
		button.setText("Go go go!");
		button.addActionListener((e) -> {
			btnAction(e);
			
		});
		add(button, BorderLayout.WEST);
		add(pBar, BorderLayout.CENTER);
		JButton tryReadingFileButton = new JButton();
		tryReadingFileButton.setText("read");
		
		tryReadingFileButton.addActionListener((e) -> {
			String location = fileLocation.getText();
			FilterDataMain.readFileContent(location);
		});
		
		add(fileLocation, BorderLayout.SOUTH);
		add(tryReadingFileButton, BorderLayout.CENTER);
		
	}
	
	private void btnAction(ActionEvent e) {
		counter++;
		pBar.setValue(counter);
	}
	
}



