package hr.fer.oop.lab4.prob2;

public class SimpleArrayListTest {
	
	public static void main(String[] args) {
		SimpleArrayList<Integer> sal = new SimpleArrayList<>();
		sal.add(1);
		sal.add(2);
		sal.add(3);
		sal.add(4);
		System.out.println(sal.indexOf(4));
		SimpleArrayList<String> sal2 = new SimpleArrayList<>();
		sal2.add("2");
		sal2.ensureCapacity(10);
		sal2.add("1");
		sal2.add("5");
		sal2.add("3");
		sal2.add("4");
		
		System.out.println(sal2.get(3));

	}

}
