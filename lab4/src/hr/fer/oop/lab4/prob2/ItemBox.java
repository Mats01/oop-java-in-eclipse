package hr.fer.oop.lab4.prob2;

import java.time.LocalDate;

import hr.fer.oop.inheritance_polymorphism.Item;
import hr.fer.oop.inheritance_polymorphism.Perishable;

public class ItemBox<T extends Item> extends SimpleArrayList<T> {

	public ItemBox() { 
		super();
	}
	
	public SimpleArrayList<String> getPerishables(){
		SimpleArrayList<String> parList = new SimpleArrayList<String>();
		LocalDate today = LocalDate.now();
		
		for(int i = 0; i < this.size(); i++) {
			if(this.get(i) instanceof Perishable) {
				Perishable p = (Perishable) this.get(i);
				if(today.isAfter(p.getBestBefore())) {
					parList.add(this.get(i).getSku());
				}
				
					
			}
		}
		
		return parList;
	}
	
}
