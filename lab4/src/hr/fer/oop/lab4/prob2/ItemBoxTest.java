package hr.fer.oop.lab4.prob2;

import hr.fer.oop.inheritance_polymorphism.Item;
import hr.fer.oop.inheritance_polymorphism.Milk;
import hr.fer.oop.inheritance_polymorphism.MilkType;

import java.time.LocalDate;
import java.time.Period;

import hr.fer.oop.inheritance_polymorphism.Beverage;

public class ItemBoxTest {
	public static void main(String[] args) {
	    ItemBox<Beverage> a = new ItemBox<>();
		Beverage b = new Beverage("23", "Coca cola", 10, 2);		
		Milk m = new Milk("123", "a", 6.99, 1.0, MilkType.COW, LocalDate.now().minusDays(-2));
		a.add(b);
		//a.add(m);
		a.getPerishables();

		System.out.println(a.getPerishables().size());
		SimpleArrayList<String> result = a.getPerishables();
		System.out.println(result.get(1));

	}
	
	public int lol(int a) {
		int b;
		if(a == 0) {
			b = 3;
		} else {
			b = 2;
		}
		return b;
	}

}
