package hr.fer.oop.lab4.edgar2;

public class InvalidPeppaPigCharacterException extends RuntimeException {
	
	public InvalidPeppaPigCharacterException() {
		super();
	}
	public InvalidPeppaPigCharacterException(String m) {
		super(m);
	}
 
}
