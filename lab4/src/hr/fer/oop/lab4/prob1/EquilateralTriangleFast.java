package hr.fer.oop.lab4.prob1;

import hr.fer.oop.lab3.pic.Picture;


/**
 * 
 * Way better than equilateral triangle
 * fills in the whole triangle
 * 
 * @author Matej Butkovic
 */
public class EquilateralTriangleFast extends EquilateralTriangle {
	
	public EquilateralTriangleFast(int height_, int base_) {
		super(height_, base_);
	}
	
	/**
	 * calculates lines on which the triangle's sides lie, 
	 * 
	 * draws a line throught those points
	 * 
	 * efficient!
	 * 
	 * can only be drawn in the center 
	 * 
	 * finally translates coordiantes to the center of the picture
	 * 
	 */
	//@Override
	public void drawOnPicture(CustomPicture p, int originX, int originY) {
		int x0 = originX - this.base /2;
		int x1 = x0 + this.base;
		this.endHeightY = originY - this.height;
		try {
			p.drawLine(p.restrictWidth(x0), p.restrictWidth(x1), p.restrictHeight(originY));	
		}
		catch(IllegalArgumentException e){}
 		
 		
 		int ya = 0;
 		int xa = -1* this.base/2;
 		int xb = 0;
 		int yb = this.height;
 		float k1 = (float) (yb-ya)/(xb-xa);
 		int xa2 = this.base/2;
 		float k2 = (float) (yb-ya)/(xb-xa2);
 		int xLeft = 0;
 		int xRight = 0;
 		for(int y = 0; y <= yb; y++) { //goes from top to bottom
 			xLeft = (int) ((int) (y - ya + k1*xa)/k1);
			xRight = (int) ((int) (y - ya + k2*xa2)/k2);
 			
 			try {
 				
 				p.drawLine(p.restrictWidth(xLeft+originX), p.restrictWidth(xRight+originX), p.restrictHeight(originY-y)); //translates coordinates to the center of the screen	
 			}
 			catch(IllegalArgumentException e){}
 			
 			
 		}
	}

}
