package hr.fer.oop.lab4.prob1;

import hr.fer.oop.lab3.pic.Picture;

/**
 * I need this so all my restricions would be in one place
 * 
 * 
 * @author Matej Butkovic
 */
public class CustomPicture extends Picture {

	public CustomPicture(int width, int height) {
		super(width, height);

	}
	
	public int restrictWidth(int val) {
		if(val < 0) {
			return 0;
		}
		if (val > this.getHeight()) {
			val =  this.getWidth()-1;
			return val;
		}
		return val;
	}
	public int restrictHeight(int val) {
		if(val < 0) {
			return 0;
		}
		if (val > this.getHeight()) {
			return this.getHeight()-1;
		}
		return val;
	}

}
