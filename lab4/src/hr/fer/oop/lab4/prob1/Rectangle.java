package hr.fer.oop.lab4.prob1;

import hr.fer.oop.lab3.pic.Picture;
import hr.fer.oop.lab3.pic.PictureDisplay;


/**
 * 
 * a rectangle
 * 
 * @author Matej
 */
public class Rectangle {
	int a;
	int b;
	
	Rectangle(int a_, int b_){
		this.a = a_;
		this.b = b_;
	}
	
	/**
	 * draws on picture
	 * @param p
	 */
	public void drawOnPicture(CustomPicture p, int originX, int originY) {
		
		
		for(int y = originY-b/2; y <= originY+b/2; y++) {
			p.drawLine(p.restrictWidth(originX - a/2), p.restrictWidth(originX+a/2), p.restrictHeight(y));
		}
	}

}
