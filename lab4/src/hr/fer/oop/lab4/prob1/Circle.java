package hr.fer.oop.lab4.prob1;

import hr.fer.oop.lab3.pic.Picture;
import hr.fer.oop.lab3.pic.PictureDisplay;

import java.lang.Math;

/**
 * Circle draws a circle on a picture
 * 
 * @author Matej Butkovic
 */
public class Circle {
	int circeRaius;
	
	Circle(int rad){
		this.circeRaius = rad;
	}
	/**
	 * draws on picture
	 * @param p this is the picture it draws onto
	 */
	public void drawOnPicture(CustomPicture p) {
		int oririginX = p.getWidth()/2;
		int originY = p.getHeight()/2;
		for(int x = -this.circeRaius-1; x < this.circeRaius+1; x++) {		
			for(int y = -this.circeRaius-1; y < this.circeRaius+1; y++) {
				if( Math.sqrt(x*x + y*y)  <=  this.circeRaius) {
					try {
						p.turnPixelOn(x+oririginX, originY-y);		
						}
						catch(IllegalArgumentException e){}
					
				}
				
			}
		}
		
	}
}
