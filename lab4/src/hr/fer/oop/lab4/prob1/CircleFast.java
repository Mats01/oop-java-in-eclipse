package hr.fer.oop.lab4.prob1;

import hr.fer.oop.lab3.pic.Picture;
import hr.fer.oop.lab3.pic.PictureDisplay;


/**
 * The CircleFast class has little to do with circle
 * it relays on the drawLine function from the Picture class render the circle
 * 
 * 
 * @author matej
 */
public class CircleFast extends Circle {
	
	public CircleFast(int r) {
		super(r);
	}
	
	
	/** 
	 * Starts from the top of the circle and goes down the y-axis
	 * looking for x-values on the circle left and then right of the y-axis in separate loops
	 * checking only a few x-s for each y
	 * 
	 * finally it draws a line between the two points it found using the drawLine function
	 * 
	 * it can also draw this circle wherever you want
	 * 
	 * 
	 * @see hr.fer.oop.lab4.prob1.Circle#drawOnPicture(hr.fer.oop.lab3.pic.Picture)
	 */
	//@Override
	public void drawOnPicture(CustomPicture p, int originX, int originY) {
		int x0n = 0;
		int x1n = 0;
		for(int y = -this.circeRaius; y < this.circeRaius; y++) { //goes from the top to the bottom of the circle
			x0n = 0;
			x1n = 0;
			for(int x = x0n; x >  -this.circeRaius; x--) { //left side of the circle
				
				if( Math.sqrt(x*x + y*y)  >=  (this.circeRaius - 1) && Math.sqrt(x*x + y*y)  <=  this.circeRaius) {
					if(x != x0n) {
						x0n = x;
						break;
					}
				}
			}
			for(int x = x1n; x <  this.circeRaius; x++) { //right side of the circle
				
				if( Math.sqrt(x*x + y*y)  >=  (this.circeRaius - 1) && Math.sqrt(x*x + y*y)  <=  this.circeRaius) {
					if(x != x1n) {
						x1n = x;
						break;
					}
				}
			}
			
			try {
				
 				p.drawLine(p.restrictWidth(x0n + originX), p.restrictWidth(x1n + originX), p.restrictHeight(originY-y)); //translates coordinates to the center of the screen	
 				
 				
 			}
 			catch(IllegalArgumentException e){}
		}
	}
	

}
