package hr.fer.oop.lab4.prob1;


import hr.fer.oop.lab3.pic.PictureDisplay;


/**
 * 
 * @author Matej Butkovic
 */
public class Demonstration {
	public static void main(String[] args) {
		EquilateralTriangle et = new EquilateralTriangle(300,600);
		CustomPicture p = new CustomPicture(1000, 1000);
		//et.drawOnPicture(p);
		
		Circle c = new Circle(300);
		//c.drawOnPicture(p);
		
		Rectangle r = new Rectangle(300, 200);
		//r.drawOnPicture(p, 250, 150);
		
		EquilateralTriangleFast etf = new EquilateralTriangleFast(600, 100);
		etf.drawOnPicture(p, p.getWidth()/2+100, p.getHeight()/2+200);
		
		CircleFast cf = new CircleFast(200);
		//cf.drawOnPicture(p, 220, 760);
		
		PictureDisplay.showPicture(p);
	
	}

}
