package hr.fer.oop.lab4.prob1;

import hr.fer.oop.lab3.pic.Picture;
import hr.fer.oop.lab3.pic.PictureDisplay;


/**
 * 
 * uses the formula for a line through 2 points to calculate which pixels to turn on
 * 
 * @author Matej Butkovic
 */
public class EquilateralTriangle {
	int height;
	int base;
	int beginBaseX;
	int endBaseX;
	int endHeightY;
	EquilateralTriangle(int height_, int base_) {
		this.height = height_;
		this.base = base_;
		this.endBaseX = this.beginBaseX + this.base;
		
		

		
	}
	/**
	 * draws triangle on picture
	 * @param p
	 */
	public void drawOnPicture(CustomPicture p) {
		int oririginX = p.getWidth()/2;
		int originY = p.getHeight()/2;
		int x0 = oririginX - this.base /2;
		int x1 = x0 + this.base;
		this.endHeightY = originY - this.height;
		try {
			p.drawLine(p.restrictWidth(x0), p.restrictWidth(x1), p.restrictHeight(originY));
		}
		catch(IllegalArgumentException e){}
 		
 		
 		int ya = 0;
 		int xa = -1* this.base/2;
 		int xb = 0;
 		int yb = this.height;
 		float k1 = (float) (yb-ya)/(xb-xa);
 		int xa2 = this.base/2;
 		float k2 = (float) (yb-ya)/(xb-xa2);
 		System.out.println(k1);
 		int xLeft = 0;
 		int xRight = 0;
	
	
		for(int y = 0; y < yb; y++) {
			xLeft = (int) ((int) (y - ya + k1*xa)/k1);
			xRight = (int) ((int) (y - ya + k2*xa2)/k2);
			
			for(int x = xa; x < xa2; x++) {
				if(x>= xLeft && x <= xRight) {
					try {
						p.turnPixelOn(p.restrictWidth(x+oririginX), p.restrictHeight(originY-y));
					}
					catch(IllegalArgumentException e){}			
				}
			}
		}	
	}
	
	
	
	
	

}
