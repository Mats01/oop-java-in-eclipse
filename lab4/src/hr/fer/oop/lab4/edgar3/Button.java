package hr.fer.oop.lab4.edgar3;


interface OnOffCapability {
	
	public void turnOn(boolean state);

}

class Heater {
    public void turnOn() {
        System.out.println("turnOn");
    }

    public void turnOff() {
        System.out.println("turnOff");
    }
}

class HeatingCapability implements OnOffCapability {

	
	Heater lamp;
	
	public HeatingCapability(Heater l) {
		this.lamp = l;
	}
	
	@Override
	public void turnOn(boolean state) {
		if(state) {
			this.lamp.turnOn();
		} else {
			this.lamp.turnOff();
		}
		
	}

}

class Lamp {
    public void light() {
        System.out.println("light");
    }

    public void dark() {
        System.out.println("dark");
    }
}
class LampCapability implements OnOffCapability {
	
	Lamp lamp;
	
	public LampCapability(Lamp l) {
		this.lamp = l;
	}
	
	@Override
	public void turnOn(boolean state) {
		if(state) {
			this.lamp.light();
		} else {
			this.lamp.dark();
		}
		
	}

}

public class Button {
	
	
	boolean state = false;
	OnOffCapability capability;
	
	public Button(OnOffCapability ooc) {
		this.capability = ooc;
	}
	
	public void click() {
		this.state = !this.state;
		this.capability.turnOn(this.state);
	}
}
