package hr.fer.oop.lab5.prob2;

import static org.junit.Assert.*;

import org.junit.jupiter.api.Test;

class MyNumberTest2 {

	@Test
	void canCreateMyNumber() {
		MyNumber num = new MyNumber(123);
		
		assertEquals("Worng value saved", Double.valueOf(123), Double.valueOf(num.value));
	}
	
	@Test
	void pritingMyNumber() {
		MyNumber num = new MyNumber(123);
		for(Integer i : num){
			System.out.format("%d ", i);
		}
	}
	
	@Test
	void addingToIntegerSet() {
		IntegerSet set = new IntegerSet();
		set.add(157);
		set.add(12345);
		set.add(-3005);
	}
	
	@Test
	void printingIntegerSet() {
		IntegerSet set = new IntegerSet();
		set.add(157);
		set.add(12345);
		set.add(-3005);
		System.out.println();
		for(MyNumber numbers : set){
			System.out.format("Broj %s ima znamenke:", numbers.toString());
			for(Integer i : numbers){
			System.out.format(" %d", i);
			}
			System.out.println();
			}
	}
	

}
