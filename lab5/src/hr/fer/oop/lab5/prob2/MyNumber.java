package hr.fer.oop.lab5.prob2;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * 
 * Custom type of integer which can iterate through it's digits
 * 
 * @author Matej Butkovic
 */
public class MyNumber implements Iterable<Integer>{
	Integer value;
	private LinkedList<Integer> digits = new LinkedList<>();
	public MyNumber(Integer val) {
		this.value = val;
		
		if(val < 0) {
			val *=-1;
		}
		int x = val;
		do {
			digits.addFirst(x % 10);
			x = x/10;
			
		}while(x > 0);
		
	}
	@Override
	public Iterator<Integer> iterator() {
		return digits.iterator();
	}
	@Override
	public String toString() {
		return String.valueOf(this.value);
	}
	
}
