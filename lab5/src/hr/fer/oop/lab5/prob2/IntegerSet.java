package hr.fer.oop.lab5.prob2;

import java.util.Iterator;
import java.util.*;

/**
 * allows iteration through custom class MyInteger
 * 
 * @author Matej Butkovic
 */
public class IntegerSet implements Iterable<MyNumber> {
	Set<Integer> set = new TreeSet<>();
	
	public void add(int num) {
		set.add(num);
	}

	@Override
	public Iterator<MyNumber> iterator() {
		return new SetIterator();
	}
	
	private class SetIterator implements Iterator<MyNumber> {
		
		Iterator<Integer> iterator;
		
		public SetIterator() {
			iterator = set.iterator();
		}

		@Override
		public boolean hasNext() {
			return iterator.hasNext();
		}

		@Override
		public MyNumber next() {
			if (!hasNext())	throw new NoSuchElementException();
			return new MyNumber(iterator.next());
		}
		
	}

}
