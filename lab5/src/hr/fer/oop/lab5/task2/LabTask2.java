package hr.fer.oop.lab5.task2;


import java.time.LocalDate;
import java.util.*;




class ValueThenKeyComparator<K extends Comparable<? super K>,
	V extends Comparable<? super V>>
	implements Comparator<Map.Entry<K, V>> {
	
	public int compare(Map.Entry<K, V> a, Map.Entry<K, V> b) {
	int cmp1 = a.getValue().compareTo(b.getValue());
	if (cmp1 != 0) {
	return cmp1;
	} else {
	return a.getKey().compareTo(b.getKey());
	}
	}

}

class LabTask2 {
    public static Map<String, LocalDate> orderByDate(Map<String, LocalDate> birthdays) {        
        
    	List<Map.Entry<String, Integer>> result	 = new ArrayList<Map.Entry<String, Integer>>(birthdays.entrySet());
    	Collections.sort(list, new ValueThenKeyComparator<String, Integer>());
    	return result;
    }
    
    
}