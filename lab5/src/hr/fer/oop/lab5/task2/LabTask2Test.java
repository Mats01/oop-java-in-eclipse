package hr.fer.oop.lab5.task2;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.util.*;

import org.junit.jupiter.api.Test;

class LabTask2Test {

	@Test
	void canReturnCustomMap() {
		HashMap<String, LocalDate> birthdays = new HashMap<>();
		birthdays.put("Ana", LocalDate.now());
		birthdays.put("Mija", LocalDate.now().plusDays(1));
		birthdays.put("Maja", LocalDate.now().plusDays(-1));
		Map<String, LocalDate> res = LabTask2.orderByDate(birthdays);
		System.out.println(res);
	}

}
