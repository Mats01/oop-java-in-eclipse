package hr.fer.oop.lab5.task3;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.util.*;

import org.junit.jupiter.api.Test;

class LabTask3test {

	@Test
	void test() {
		Map<Integer, Integer> map = new HashMap<>();
		map.put(0, 50);
		map.put(20, 50);
		map.put(30, 50);
		map.put(60, 50);
		map.put(80, 500);
		Set<Integer> set = new HashSet<>();
		set.add(50);
		set.add(-100);
		set.add(-200);
		set.add(-300);
		set.add(-500);
		set.add(-1000);
		set.add(0);
		set.add(500);
		set.add(5000);
		set.add(500000);
		System.out.println(LabTask3.count(map,set));
	}

}
