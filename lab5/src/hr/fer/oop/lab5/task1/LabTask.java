package hr.fer.oop.lab5.task1;

import java.util.*;


class LabTask {
    public static List<Integer> viceversa(Map<Integer, Integer> map) {
    	List<Integer> result = new ArrayList<>();
    	Iterator it = map.entrySet().iterator();
    	while(it.hasNext()) {
    		Map.Entry entry = (Map.Entry) it.next();
    		Integer key = (Integer) entry.getKey();
    		Integer value = (Integer) entry.getValue();
    		if(map.get(value) == key) {
    			result.add(value);
    				
    		}	
    	}
    	Collections.sort(result, Collections.reverseOrder());
		return result;
    	
    	
    }
    
}