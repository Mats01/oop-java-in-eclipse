package hr.fer.oop.lab5.task1;

import static org.junit.jupiter.api.Assertions.*;

import java.util.*;

import org.junit.jupiter.api.Test;

class LabTaskTest {

	@Test
	void lab1test() {
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		map.put(0,1);
		map.put(5,7);
		map.put(7,5);
		List<Integer> res = LabTask.viceversa(map);
		System.out.println(res);
		
	}

}
