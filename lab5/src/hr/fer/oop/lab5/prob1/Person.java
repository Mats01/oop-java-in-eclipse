package hr.fer.oop.lab5.prob1;
/**
 * extends bussines partner
 * identified by oib
 * 
 * @author Matej Butkovic
 */
public class Person extends BussinessPartner {
	private String name, surname;
	public Person(String oib, String name, String surname) {
		super(oib);
		this.name = name;
		this.surname = surname;
	}
	public String getName() {
		return name;
	}
	public String getSurname() {
		return surname;
	}
	
	@Override
	public String toString() {
		return String.format("%s %s - %s%n", this.name, this.surname, super.getOib());
		
	}
	
	
	
	

}
