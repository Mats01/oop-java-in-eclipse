package hr.fer.oop.lab5.prob1;

import java.util.*;
/**
 * @author Matej Butkovic
 */
public class BussinessPartnerComparator implements Comparator<BussinessPartner> {
	/**
	 * compares two business partner objects
	 * companies go first (sorted by oib)
	 * then people are listed ascending by oib
	 */
	@Override
	public int compare(BussinessPartner o1, BussinessPartner o2) {
		if (o1 instanceof Company && o2 instanceof Company) {
			int result = ((Company) o2).getEmployees().size() - ((Company) o1).getEmployees().size();
			if (result == 0){
				result = o1.getOib().compareTo(o2.getOib());
			}
			return result;
		}
		else if (o1 instanceof Company){
			return -1;
		}
		else if (o2 instanceof Company) {
			return 1;
		}
		else {
			return o1.compareTo(o2);
		}
	}
}
