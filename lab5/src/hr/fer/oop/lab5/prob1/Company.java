package hr.fer.oop.lab5.prob1;

import java.util.*;
/**
 * employees set of people employed by the company
 * identified by oib
 * 
 * @author Matej Butkovic
 */
public class Company extends BussinessPartner {
	private String name;
	private LinkedHashSet<Person> employees = new LinkedHashSet<Person>();
	public Company(String oib, String name) {
		super(oib);
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
		
	@Override 
	public String toString() {
		String result = String.format("%s - %s%n", this.name, super.getOib());
		for(Person e : employees) {
			result += String.format("    %s", e.toString());
		}
		return result;
	}

	public LinkedHashSet<Person> getEmployees() {
		return employees;
	}

}
