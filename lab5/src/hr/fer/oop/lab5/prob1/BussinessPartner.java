package hr.fer.oop.lab5.prob1;

/**
 * BussinessPartner is a comparable abstract class used as a base for both people and companies
 * 
 * @author Matej Butkovic
 */
abstract public class BussinessPartner implements Comparable<BussinessPartner> {
	/**
	 * cannot be changed later!
	 */
	private final String oib;
	
	protected BussinessPartner(String oib) {
		this.oib = oib;
	}
	
	final protected String getOib() {
		return this.oib;
	}
	
	@Override
	public int hashCode() {		
		return oib.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof BussinessPartner)
			return ((BussinessPartner) obj).oib.equals(oib);
		else
			return false;
	}	
	
	@Override
	public int compareTo(BussinessPartner o) {
		return oib.compareTo(o.oib);
	}

}
