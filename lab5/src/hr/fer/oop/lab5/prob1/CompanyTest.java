package hr.fer.oop.lab5.prob1;

import java.util.*;

public class CompanyTest {

	public static void main(String[] args) {
		Collection<BussinessPartner> col = new TreeSet<>( new BussinessPartnerComparator());
		Person p1 = new Person("42345678901", "Darko", "Darić");
		Person p2 = new Person("58855678121", "Bianka", "Banić");
		Person p3 = new Person("28964789143", "Ivan", "Ivić");
		
		Company c1 = new Company("66879845127", "Meho und jaranen");
		c1.getEmployees().add(p1);
		c1.getEmployees().add(p2);
		col.add(p1);
		col.add(p2);
		col.add(p3);
		col.add(c1);
		System.out.println(p1);
		System.out.println(c1);
		for(BussinessPartner bp : col) {
			System.out.println(bp);
		}

	}

}
