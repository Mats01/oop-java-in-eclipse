package hr.fer.oop.lab5.task4;

import java.util.*;

class Pair implements Comparable<Pair> {
    private String name1;
    private String name2;

    public Pair(String name1, String name2) {
        this.name1 = name1;
        this.name2 = name2;
    }

    public String getName1() {
        return name1;
    }

    public String getName2() {
        return name2;
    }

    @Override
    public int compareTo(Pair o) {
        int i = this.getName1().compareTo(o.getName1());
        if (i != 0) 
          return i;
        else 
          return this.getName2().compareTo(o.getName2());
    }    
}

class LabTask4 {
    public static Collection<String> getMostPopularPeople(Collection<Pair> pairs) {
    	pairs = new TreeSet<Pair>(pairs);
    	Map<String, Integer> map = new TreeMap<>();
    	for(Pair p : pairs) {
    		int curr = 1;
    		if(map.get(p.getName1()) != null) {
    			curr = map.get(p.getName1());
    			curr++;
    		} 
    		map.put(p.getName1(), curr);
    		curr = 1;
    		if(map.get(p.getName2()) != null) {
    			curr = map.get(p.getName2());
    			curr++;
    		} 
    		map.put(p.getName2(), curr);
    		
    		
    		
    	}
    	int max = 0;
    	for (Map.Entry<String, Integer> entry : map.entrySet()) {
		    if(entry.getValue() > max) {
		    	max = entry.getValue();
		    }
		}
    	TreeSet<String> result = new TreeSet<>();
    	for (Map.Entry<String, Integer> entry : map.entrySet()) {
		    if(entry.getValue() == max) {
		    	result.add(entry.getKey());
		    }
		}
    	
    	
    	
        return result;
    }
}