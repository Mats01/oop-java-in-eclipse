package hr.fer.oop.lab5.task4;

import static org.junit.jupiter.api.Assertions.*;

import java.util.*;

import org.junit.jupiter.api.Test;

class LabTask4Test {

	@Test
	void test() {
		Collection<Pair> pairs = new ArrayList<>();
		pairs.add(new Pair("ivo", "ana"));
		pairs.add(new Pair("ana", "a"));
		pairs.add(new Pair("marko", "ivo"));
		System.out.println(LabTask4.getMostPopularPeople(pairs));
	}

}
